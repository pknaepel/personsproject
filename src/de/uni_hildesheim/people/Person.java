package de.uni_hildesheim.people;

public class Person {
	String _firstName;
	String _lastName;
	Date _birthDate;
	
	public Person(String firstName, String lastName, Date birthDate) {
		_firstName = firstName;
		_lastName = lastName;
		_birthDate = birthDate;
	}
	
	public static void main(String[] args) {
		

	}
	
	public int getAge() {
		Date currentDate = new Date(17, 12, 2019);
		return currentDate.getYearDiff(_birthDate);
	}
	
	public String asString() {
		return _firstName + " " + _lastName + " " + _birthDate.asString();
	}
}
