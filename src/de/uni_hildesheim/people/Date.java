package de.uni_hildesheim.people;

public class Date {

	int _day;
	int _month;
	int _year;

	public Date(int day, int month, int year) {
		_day = day;
		_month = month;
		_year = year;
	}
	
	public static void main(String[] args) {
		

	}
	
	public String asString() {
		return _day + "." + _month + "." + _year;
	}
	
	public int getYearDiff(Date date) {
		int diff = date._year - _year;
	    if (date._month > _month || 
	        (date._month == _month && date._day > _day)) {
	        diff--;
	    }
	    return diff * -1;
	}
}
