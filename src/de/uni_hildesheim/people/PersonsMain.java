package de.uni_hildesheim.people;

public class PersonsMain {

	public static void main(String[] args) {
		Person[] persons = new Person[5];

		System.out.print("Lege Personen an ...\n");
		printFilledPersonCount(persons);

		persons[0] = new Person("Peter", "M�ller", new Date(10, 10, 1965));
		persons[1] = new Person("Fritz", "Bergmeier", new Date(20, 8, 1996));

		printFilledPersonCount(persons);

		persons[2] = new Person("Vincent", "Golonka", new Date(8, 9, 1980));
		persons[3] = new Person("Helge", "Bothmann", new Date(4, 2, 2002));
		persons[4] = new Person("Bob", "Baumeister", new Date(18, 12, 2020));

		printFilledPersonCount(persons);
		System.out.print("Folgende Personen wurden angelegt:\n");

		for(int i = 0; i < persons.length; i++) {
			System.out.print(persons[i].asString() + " " + persons[i].getAge() + "\n");
		}
	}

	public static void printFilledPersonCount(Person[] persons) {

		System.out.print("Anzahl der angelegten Personen: " + getFilledCount(persons) + "\n");
	}

	public static int getFilledCount(Person[] persons) {
		int filledCount = 0;
		for (int i = 0; i < persons.length; i++) {
			if(persons[i] != null)
			{
				filledCount++;
			}
		}
		return filledCount;
	}

}
